import { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Formulario from '../../components/Formulario';

import PersonContext from '../../context/persons/PersonContext';

const AgregarPersona = () => {
  const { agregarPersona, addOk } = useContext(PersonContext);
  let navigate = useNavigate();

  useEffect(() => {
    if (addOk) {      
      navigate('/dashboard');
    }
  }, [addOk, navigate]);

  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>Agregar Nueva Persona</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
          <Formulario agregarPersona={agregarPersona} />
        </div>
      </div>
    </div>
  );
};

export default AgregarPersona;
