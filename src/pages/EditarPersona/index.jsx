import React, { useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import PersonsContext from '../../context/persons/PersonContext';
import Formulario from '../../components/Formulario';

const EditarPersona = () => {
  const params = useParams();
  const navigate = useNavigate();    
  let isViewMode = params.isView === "true" ? true : false;

  const { obtenerPersona, editarPersona, persona, editOk } =
    useContext(PersonsContext);

  useEffect(() => {
    obtenerPersona(params.editId);
  }, []);

  useEffect(() => {
    if (editOk) {
      navigate('/dashboard');
    }
  }, [editOk]);

  const tienePersona = Object.keys(persona).length;

  return (
    <div className='row'>
      <div className='col-12'>
        <h5 className='text-primary text-center py-3'>{ isViewMode ? 'Ver Detalle de Persona' : 'Editar Persona' }</h5>
        <div className='shadow-lg p-3 mb-5 bg-body rounded'>
          {tienePersona > 0 && (
            <Formulario
              persona={persona}
              editar={true}              
              isModeView={isViewMode}
              editarPersona={editarPersona}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default EditarPersona;
