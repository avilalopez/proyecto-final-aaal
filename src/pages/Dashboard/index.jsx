import { useContext, useEffect } from 'react';
import Table from '../../components/Table';
import PersonsContext from '../../context/persons/PersonContext';
const Dashboard = () => {
  const { listaPersonas, deleteOk, obtenerPersonas, eliminarPersona } =
    useContext(PersonsContext);

  useEffect(() => {
    obtenerPersonas();
  }, []);

  useEffect(() => {
    if (deleteOk) {
      obtenerPersonas();
    }
  }, [deleteOk]);

  return (
    <>
      <h1>Listado de Personas</h1>
      <Table
        listaPersonas={listaPersonas}
        eliminarPersona={eliminarPersona}
      />
    </>
  );
};

export default Dashboard;
