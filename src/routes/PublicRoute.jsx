import { useContext } from 'react';
import { Navigate } from 'react-router-dom';

import AuthContext from '../context/auth/AuthContext';

const PublicRoute = ({ children }) => {
  const {
    state: { isAuth, consultando },
  } = useContext(AuthContext);
  if (consultando) return <h1>Cargando ...</h1>
  return <>{isAuth ? <Navigate to='/dashboard' /> : children}</>;
};

export default PublicRoute;
