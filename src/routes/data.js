import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

import Loigin from '../pages/Login';
import Dashboard from '../pages/Dashboard';
import AgregarPersona from '../pages/AgregarPersona';
import EditarPersona from '../pages/EditarPersona';

export const listRoute = [
  {
    path: '/',
    element: (
      <PublicRoute>
        <Loigin />
      </PublicRoute>
    ),
  },
  {
    path: '/dashboard',
    element: (
      <PrivateRoute>
        <Dashboard />
      </PrivateRoute>
    ),
  },
  {
    path: '/agregar-persona',
    element: (
      <PrivateRoute>
        <AgregarPersona />
      </PrivateRoute>
    ),
  },
  {
    path: '/editar-persona/:editId/:isView',    
    element: (
      <PrivateRoute>
        <EditarPersona />
      </PrivateRoute>
    ),
  },
  {
    path: '*',
    element: (
      <div>
        <p>No se encontró la página consultada</p>
      </div>
    ),
  },
];
