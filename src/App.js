import Rutas from './routes';
import AuthState from './context/auth/AuthState';
import PersonState from './context/persons/PersonState';

function App() {
  return (
    <AuthState>
      <PersonState>
        <Rutas />;
      </PersonState>
    </AuthState>
  );
}

export default App;
