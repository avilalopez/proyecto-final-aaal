import React from 'react';
import { Link } from 'react-router-dom';

const RowTable = ({ info, indice, eliminar }) => {
  const { id, nombres, apellidopaterno, apellidomaterno, genero, edad } = info;
  return (
    <tr>
      <th scope="row">{indice + 1}</th>
      <td>{nombres}</td>
      <td>{apellidopaterno}</td>
      <td>{apellidomaterno}</td>
      <td>{genero}</td>
      <td>{edad}</td>
      <td className="d-flex justify-content-around">
        <Link className="btn btn-outline-warning" to={`/editar-persona/${id}/false`}>
          Editar
        </Link>
        <Link className="btn btn-outline-primary" to={`/editar-persona/${id}/true`}>
          Ver detalle
        </Link>        
        <button className="btn btn-outline-danger" onClick={eliminar}>
          Eliminar
        </button>
      </td>
    </tr>
  );
};

export default RowTable;
