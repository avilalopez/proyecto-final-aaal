import React from 'react';
import RowTable from '../RowTable';

const Table = ({ listaPersonas, eliminarPersona }) => {
  return (
    <table className='table'>
      <thead>
        <tr>
          <th scope='col'>#</th>
          <th scope='col'>Nombres</th>
          <th scope='col'>Apellido Paterno</th>
          <th scope='col'>Apellido Materno</th>
          <th scope='col'>Genero</th>
          <th scope='col'>Edad</th>
          <th className='text-center' scope='col'>
            Acciones
          </th>
        </tr>
      </thead>
      <tbody>
        {listaPersonas.map((item, index) => (
          <RowTable
            key={item.id}
            info={item}
            indice={index}
            eliminar={() => eliminarPersona(item.id)}
          />
        ))}
      </tbody>
    </table>
  );
};

export default Table;
