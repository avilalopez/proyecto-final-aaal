import { useState, useContext } from 'react';

import AuthContext from '../../context/auth/AuthContext';

const LoginForm = () => {
  const [formValues, setFormValues] = useState({
    email: 'admin@correo.com',
    password: 'admin.123456',
  });

  const { iniciarSesion } = useContext(AuthContext);

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const { email, password } = formValues;    
    iniciarSesion(email, password);
  };

  return (
    <form className='border border-1 p-5' onSubmit={handleSubmit}>
      <h2 className='p-3 text-center'>Hola</h2>
      <h4 className='p-3 text-center'>Ingrese su dirección de correo electrónico y contraseña para acceder al panel de administración.</h4>
    
      <div className='mb-3'>
        <label htmlFor='email' className='form-label'>
          Correo
        </label>
        <input
          type='email'
          className='form-control'
          name='email'
          onChange={handleChange}
          value={formValues.email}
        />
      </div>
      <div className='mb-3'>
        <label htmlFor='password' className='form-label'>
          Contraseña
        </label>
        <input
          type='password'
          className='form-control'
          name='password'
          onChange={handleChange}
          value={formValues.password}
        />
      </div>

      <div className='d-grid gap-2 mx-auto'>
        <button type='submit' className='btn btn-primary'>
          Iniciar Sesión
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
