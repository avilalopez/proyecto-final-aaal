import React, { useEffect, useState } from "react";
import Error from "./Error";

const initialState = {
  nombres: "",
  apellidopaterno: "",
  apellidomaterno: "",
  genero: "",
  edad: "",
  resenhia: "",
};

const errorInit = {
  ...initialState,
};

const generos = ["Masculino", "Femenino", "Otro"];

const Formulario = ({
  agregarPersona,
  persona,
  editar = false,
  editarPersona,
  isModeView = false,
}) => {
  const [dataForm, setDataForm] = useState(initialState);
  const [errors, setErrors] = useState(errorInit);
  
  useEffect(() => {
    if (editar) {
      setDataForm({ ...persona });
    }
  }, []);

  const handleChange = (e) => {
    setDataForm({ ...dataForm, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: "" });
  };

  const isValid = () => {
    let respuesta = true;
    const localErrors = { ...errorInit };
    for (let key in dataForm) {
      if (key !== "id") {
        if (dataForm[key].trim() === "" || dataForm[key].length === 0) {
          localErrors[key] = "Campo requerido";
          respuesta = false;
        }
      }
    }

    setErrors({ ...localErrors });

    return respuesta;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isValid()) {
      if (editar) {
        console.log('editarPersona')
        editarPersona(dataForm);
      } else {
        console.log('agregarPersona')
        agregarPersona(dataForm);
      }
    }
  };

  return (
    <form className="row g-3" onSubmit={handleSubmit}>
      <div className="col-md-6">
        <label htmlFor="nombres" className="form-label">
          Nombre(s)
        </label>
        <input
          id="nombres"
          type="text"
          className="form-control"
          name="nombres"
          onChange={handleChange}
          value={dataForm.nombres}
        />
        <Error text={errors.nombres} />
      </div>
      <div className="col-md-6">
        <label htmlFor="apellidopaterno" className="form-label">
          Apellido Paterno
        </label>
        <input
          id="apellidopaterno"
          type="text"
          className="form-control"
          name="apellidopaterno"
          onChange={handleChange}
          value={dataForm.apellidopaterno}
        />
        <Error text={errors.apellidopaterno} />
      </div>
      <div className="col-md-6">
        <label htmlFor="apellidomaterno" className="form-label">
          Apellido Materno
        </label>
        <input
          id="apellidomaterno"
          type="text"
          className="form-control"
          name="apellidomaterno"
          onChange={handleChange}
          value={dataForm.apellidomaterno}
        />
        <Error text={errors.apellidomaterno} />
      </div>
      <div className="col-md-3">
        <label htmlFor="genero" className="form-label">
          Genero
        </label>
        <select
          id="genero"
          className="form-select"
          name="genero"
          onChange={handleChange}
          value={dataForm.genero}
        >
          <option disabled value="">
            Selecciona un Genero
          </option>
          {generos.map((genero) => (
            <option key={genero} value={genero}>
              {genero}
            </option>
          ))}
        </select>
        <Error text={errors.genero} />
      </div>
      <div className="col-md-3">
        <label htmlFor="edad" className="form-label">
          Edad
        </label>
        <input
          id="edad"
          type="number"
          className="form-control"
          name="edad"
          onChange={handleChange}
          value={dataForm.edad}
        />
        <Error text={errors.edad} />
      </div>
      <div className="col-md-12">
        <label htmlFor="resenhia" className="form-label">
          Breve Reseña
        </label>
        <textarea
          className="form-control"
          id="resenhia"
          rows="3"
          name="resenhia"
          onChange={handleChange}
          value={dataForm.resenhia}
        ></textarea>
        <Error text={errors.resenhia} />
      </div>
      <div className="col-6">      
      {
        !isModeView  && <button type="submit" className="btn btn-success">          
        {editar ? "Editar" : "Crear"}
      </button>
      }
      </div>
    </form>
  );
};

export default Formulario;
