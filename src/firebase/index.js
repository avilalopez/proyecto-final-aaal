import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBS-DUu-FHp-NJ6Jlcr4MiBhH4y6n1YHHc",
  authDomain: "crud-personas-23ee0.firebaseapp.com",
  projectId: "crud-personas-23ee0",
  storageBucket: "crud-personas-23ee0.appspot.com",
  messagingSenderId: "28768101369",
  appId: "1:28768101369:web:9d75718ff87b80b3135328"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export { db, firebase };
