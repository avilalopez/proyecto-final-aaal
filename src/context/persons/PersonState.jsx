import { useReducer } from 'react';
import PersonContext from './PersonContext';
import PersonReducer from './PersonReducer';

import { db } from '../../firebase';

const PersonState = ({ children }) => {
  const initialState = {
    listaPersonas: [],
    persona: {},
    addOk: false,
    deleteOk: false,
    editOk: false,
    verOK: false,
  };

  const agregarPersona = async (persona) => {
    try {
      await db.collection('personas').add(persona);
      dispatch({
        type: 'AGREGAR_PERSONA',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerPersonas = async () => {
    try {
      const personas = [];
      const info = await db.collection('personas').get();
            
      info.forEach((item) => {
        personas.push({
          id: item.id,
          ...item.data(),
        });
      });
      
      dispatch({
        type: 'LLENAR_PERSONAS',
        payload: personas,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const eliminarPersona = async (id) => {
    try {
      await db.collection('personas').doc(id).delete();
      dispatch({
        type: 'ELIMINAR_PERSONA',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerPersona = async (id) => {
    try {
      const info = await db.collection('personas').doc(id).get();
      
      let persona = {
        id: info.id,
        ...info.data(),
      };
      dispatch({
        type: 'OBTENER_PERSONA',
        payload: persona,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const editarPersona = async (persona) => {
    
    try {
      const personUpdate = { ...persona };
      delete personUpdate.id;

      await db.collection('personas').doc(persona.id).update(personUpdate);
      dispatch({
        type: 'EDITAR_PERSONA',
        payload: true,
      });
    } catch (error) {}
  };

  const [state, dispatch] = useReducer(PersonReducer, initialState);
  return (
    <PersonContext.Provider
      value={{
        listaPersonas: state.listaPersonas,
        addOk: state.addOk,
        deleteOk: state.deleteOk,
        persona: state.persona,
        editOk: state.editOk,
        agregarPersona,
        obtenerPersonas,
        eliminarPersona,
        obtenerPersona,
        editarPersona,
      }}
    >
      {children}
    </PersonContext.Provider>
  );
};

export default PersonState;
