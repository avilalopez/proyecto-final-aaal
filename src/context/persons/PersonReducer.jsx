const reducer = (state, action) => {
  switch (action.type) {
    case 'AGREGAR_PERSONA':
      return {
        ...state,
        addOk: action.payload,
      };
    case 'LLENAR_PERSONAS':
      return {
        ...state,
        addOk: false,
        deleteOk: false,
        editOk: false,
        persona: {},
        listaPersonas: action.payload,
      };
    case 'ELIMINAR_PERSONA':
      return {
        ...state,
        deleteOk: action.payload,
      };
    case 'OBTENER_PERSONA':
      return {
        ...state,
        persona: action.payload,
      };
    case 'EDITAR_PERSONA':
      return {
        ...state,
        editOk: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
